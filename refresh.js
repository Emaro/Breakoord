
// ? Does alerted really need to be initialized as true?
var alerted = true;
var clock = document.getElementById("clock");

function timer()
{
    var now = new Date();
    var startTime = new Date(clock.getAttribute('data-since').replace(" ","T"));

    var timespanInMs = now.getTime() - startTime.getTime();
    var timespanInMin = timespanInMs / (1000 * 60);

    clock.innerHTML = "Du bist seit " + timespanInMin.toFixed(1) + " Minuten in der Pause";
    if (!alerted && timespanInMin > 15)
    {
        alerted = true;
        alert("Vergiss nicht, dich wieder auszuchecken");    
    }
}

if (clock != null) {
    timer();
    var x = setInterval(timer, 500);
}

// Update the -number-of-users-in-pause counter
function getUserCount()
{
    // TODO: Replace XHR with fetch
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            if (c != this.responseText)
            {
                window.location.assign("");
            }
            document.getElementById("uCount").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "app/ajax/countUserInPause.php", true);
    xhttp.send();
}

// Update the 
function getUsers()
{
    // TODO: Replace XHR with fetch
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("uList").outerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "app/ajax/userInPauseList.php", true);
    xhttp.send();
}

getUserCount();
var y = setInterval(getUserCount, 3000);
getUsers();
var z = setInterval(getUsers, 3000);