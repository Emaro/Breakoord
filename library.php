<?php

if (!defined('HOME')) {
    define('HOME', './');
}

require HOME . 'config.php';
require CONNECTION;
require HELP_FUNCTIONS;
require LIBRARY;
require UPDATE_METHODS;
require FUNCTIONS;
require MAILER;
