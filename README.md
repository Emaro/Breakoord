# Breakoord

Breakoord (previously named Pausenmanager in german) is a web application to coordinate breaks in an environment where not everyone can take a break at the same time. To be more specific, administrators can define time slots for breaks and limit the amount of people that can take a break at the same time. Users can check out while taking a break, as long as there's an available slot.

![Screenshot](etc/screenshot.png)

## Features

- Define time slots and how many people can take a break at that time.
- People can assign themself a shift, so one can let the workers from earlier shifts take their break first (enforceable).
- People can check out and in for their break.
- Whiteboard with live updates and overview over the whole team.
- User management: create, update and remove user accounts.
- Analytics: download raw data to analyse if someone exploits the break time.

Note: The language of the user interface is german with a little bit of swiss german.

## Setup

Create the breakoord database by executing `etc/breakoord.sql`. I recommend you to create a database user which can only access the breakoord database. Copy or rename `example.config.php` to `config.php` and set `DB_NAME`, `DB_USER` and `DB_PASS` according to your configuration. It's expected that the database runs on the same host as the Breakoord PHP application, i.e. the application looks for the database on localhost.

If you run the application in a managed web hosting environment, copy the entire repository into any directory in your web root. You can also start the application yoursely with the php cli with the command `php -S localhost:7979 -t .`.

Next, navigate to the application in your browser. The database is initialized with an admin user. Login with the email `admin@example.com` and the password `admin`.

**Go to *Account* and change the default password! DO NOT SKIP THIS STEP.**

Now you can create user accounts in the *Administration* section and configure the break slots.