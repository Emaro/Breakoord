<?php

// Session
session_start();

// Relative path to the root directory
define('HOME', './');
require HOME . 'library.php';
// Redirect guests to the login page
require REDIR_GUESTS;

// Generate a new CSRF token
refreshToken();

// Perform all updates
$currentUser = currentUser();
if (updateState($currentUser) || updateShift($currentUser))
    refreshSite();


// Print HTML Head
head();

h1("Übersicht");

printMenuCore();


h2("Pause");

$pauseMessage = pauseAllowed();
$msg = $pauseMessage['msg'];
$dis = $pauseMessage['dis'] ? ' disabled' : '';
$msg = $msg ? "($msg)" : '';

printPauseButton($msg, $dis);

printShiftForm();


h2("Whiteboard");

printUserInPauseCounter();

$userLists = createUserLists();

echo '<div class="flex">' . PHP_EOL;
printUserList($userLists['can'], "No z'guet");
printUserInPauseList();
printUserList($userLists['done'], "Scho gsi");
printUserList($userLists['else'], "Nid am schaffe");
echo '</div>';

?>

<!-- Scripts -->

<script> var c = <?=count(User::inPause())?>; </script>
<script src="refresh.js"></script>
