<?php

session_start();

define("HOME", "./");

require HOME . 'library.php';

head();

h1("Info");

if (currentEmail())
    printMenuCore();
else
    printMenu([a(HOME, 'Zurück')]);

h2("Dokumentation");

?>

<p><a class="download" href="doc/Dokumentation Pausenmanager (2020.v2).pdf" download>Dokumentation herunterladen (PDF)</a></p>

<h2>Kontakt</h2>

<p>Für Fragen bezüglich Änderungen der Kontodaten (inkl. Passwort-Reset) sind die Supervisors zuständig.</p>

<p>Der Pausenmanager wurde von <a href="mailto:Miro Albrecht <breakoord@emaro.dev>">Miro Albrecht</a> entwickelt.</p>