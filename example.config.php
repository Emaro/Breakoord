<?php

/**
 * RENAME THIS FILE TO config.php!
 */

require_once HOME . 'app/appConfig.php';

// Zugangsdaten Datenbank (mysql, per localhost erreichbar)
define("DB_NAME", '');
define("DB_USER", '');
define("DB_PASS", '');

// Email-Adresse von Breakoord, welche bei automatischen Benachrichtigungen als Absender verwendet wird
define("SENDER_EMAIL_ADDRESS", "");

// Sperrt Pause, wenn andere Benutzer eine höhere Priorität (Schicht) haben
define("PRIORITY_BLOCKS", false);

// Mindestlänge für (neue) Passwörter
define("PW_MIN_LENGTH", 10);
