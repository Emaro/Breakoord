<?php
session_start();

// Required by all the imports
define("HOME", "./");

// Imports
require HOME . 'library.php';

// Redirect if user is already logged in
require REDIR_USERS;

// Sign the user in if requested by the form. Redirects to the overview page on success.
$loginError = login();

head();
?>

<h1>Pausenmanager</h1>

<p>Willkommen beim Pausenmanager vom Service Desk BIT.
<br><a href="info.php">Mehr Informationen</a></p>

<h2>Login</h2>

<? p($loginError) ?>

<form action="" method="post">
    <input type="text" name="email" placeholder="E-Mail"><br>
    <input type="password" name="password" placeholder="Passwort"><br>
    <input type="submit" name="login" value="Anmelden">
    <input type="hidden" name="token" value="<?= token() ?>">
</form>