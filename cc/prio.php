<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

foreach (State::getAll() as $state)
{
    foreach(Shift::getAll() as $shift)
    {
        $pn = $_POST[$shift->id() . "_" . $state->id()];
        if (isset($pn))
        {
            if ($p = Priority::find($shift, $state) and $p !== null)
            {
                if ($pn == 0)
                {
                    $p->delete();
                }
                else
                {
                    $p->setPriority($pn);
                    $p->update();
                }
            }
            elseif ($pn != 0)
            {
                Priority::new($pn, $shift, $state);
            }
        }
    }
}

head();

h1("Prioritäten");

printAdminMenu();


echo "<form action=\"\" method=\"post\"><table>";

echo "<tr><th></th>";
foreach (State::getAll() as $state)
{
    echo "<th>".$state->label()."</th>";
}
echo "</tr>";

foreach (Shift::getAll() as $shift)
{
    echo "<tr>";
    echo "<th>".$shift->label()."</th>";
    foreach (State::getAll() as $state)
    {
        echo "<td>";
        $p = Priority::find($shift, $state);
        echo "<input type=\"number\" name=\"". $shift->id() . "_" . $state->id()
        ."\" value=\"". ($p ? $p->priority() : "0") ."\">";
        echo "</td>";
    }

    echo "</tr>";
}

echo "</table><br><input type='submit' value='Speichern'></form>";