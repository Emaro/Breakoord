<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;

if (!currentUser()->isAdmin())
{
    header("Location:" . HOME. "index.php");
    exit;
}

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="Pausenauswertung '.date("Y-m-d").'.csv"');
header("Cache-Control: no-cache, must-revalidate");
$sep = ";";

echo join($sep, ["Zeit (Start)", "Status", "Dauer (in Min)", "User"]), "\n";


foreach(User::getAll() as $user)
{
    $lastState = null;
    foreach(UserState::getFromUser($user) as $state)
    {
        $time = $state->time();
        $duration = round(minutesDiff($time, $lastState));
        if ($_REQUEST['pauseOnly']){
            if ($state->state()->id() == 2)
            {
echo join($sep, [$time, $state->state()->label(), $duration, $state->user()->email()]), "\n";
            }
            
        }
        else
        {
            echo join($sep, [$time, $state->state()->label(), $duration, $state->user()->email()]), "\n";
        }
        
        $lastState = $time;
    }
}