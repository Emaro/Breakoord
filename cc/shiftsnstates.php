<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

$suc = "";

if (isset($_POST['addShift']))
{
    if (isset($_POST['label']))
    {
        $suc = Shift::insert($_POST['label']);
    }
}

if (isset($_GET['delShift']))
{
    if ($s = Shift::findById($_GET['delShift']))
    {
        $s->delete();
    }
    
    header("Location: shiftsnstates.php");
}

head();

h1("Schichten");

printAdminMenu();

foreach (Shift::getAll() as $shift)
{
    echo "<br>", $shift->label(), " (", a('?delShift='.$shift->id(), 'Del'), ")";
}

?>

<form action="" method="post">
    <input type="text" name="label" autofocus>
    <input type="submit" name="addShift" value="Add Shift">
</form>