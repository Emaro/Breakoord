<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

$suc="";

if (isset($_POST['addSlot']))
{
    if (isset($_POST['time'], $_POST['slots']))
    {
        $suc = Slot::new($_POST['time'], $_POST['slots']);
    }
}

if (isset($_GET['delSlot']))
{
    if ($s = Slot::find($_GET['delSlot']))
    {
        $s->delete();
    }
    
    header("Location: slots.php");
}

head();

h1("Pausen Slots");

printAdminMenu();

h2("Liste");

echo "<table>";

echo "<tr><th>Zeit</th><th>Anzahl Benutzer</th><th></th></tr>";

foreach(Slot::getAll() as $slot)
{
    echo "<tr><td>", $slot->time(), "</td><td>", $slot->slots(), "</td><td>", a ('?delSlot='.$slot->time(), '✕'), "</td></tr>"
    ;
}

echo "</table>";

h2("Hinzufügen");

p("Änderungen werden unmittelbar aktiv.");

if ($suc != "")
    echo "<p>", $suc ? "Gespeichert" : "Fehler";

?>

<form action="" method="post">
    <p>
    <input type="text" name="time" placeholder="Zeit (HH:MM)" autofocus>
    <input type="number" name="slots" placeholder="Anzahl Benutzer">
    <input type="submit" name="addSlot" value="Hinzufügen"></p>
</form>