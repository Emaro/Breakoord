<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

head();

echo '<base href="http://localhost:7979/cc/">';

h1("Administration");

printAdminMenu();

h2("Übersicht");

?>

<p>
<a href="analysis.php">Auswertung</a>
<br>Alle Statuswechsel aller Benutzer.</p>

<p><a href="./user.php">Benutzerverwaltung</a>
<br>Benutzer hinzufügen, bearbeiten und löschen.</p>

<p><a href="./slots.php">Pausenslots</a>
<br>Wie viele Benutzer gleichzeitig in die Pause können.</p>

<p><a href="./cleanup.php">Datenbank optimieren</a>
<br>Entferne alte Einträge, um den Speicherverbrauch der Datenbank zu verringern.</p>
