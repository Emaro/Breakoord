<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

head();

h1("Auswertung");

printAdminMenu();

h2("Downloads");

?>

<p>Alle Auswertungen enthalten Echtzeitdaten.</p>
<p><a class="download" href="dlcsv.php">Vollständig</a>
<br><a class="download" href="dlcsv.php?pauseOnly=1">Nur Pausen</a></p>