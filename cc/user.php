<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

if (isset($_POST['delUser'], $_GET['id'], $_POST['password']) && !empty($_GET['id']) && !empty($_POST['password']))
{
    if ($_SESSION[TOKEN] !== $_REQUEST[TOKEN])
    {
        header("Location: newLogin.php");
        exit;
    }
    if (currentUser()->isValidPassword($_POST['password']))
    {
        if ($user = User::findById($_GET['id']) and !empty($user))  
        {
            $user->delete();
            header("Location: user.php");
        }
    }
}



// Generate a new CSRF token
refreshToken();


head();

h1("Benutzer verwalten");

printAdminMenu();



if (isset($_GET['id']) and $user = User::findById($_GET['id']) and !empty($user))
{
    $id = $_GET['id'];
    updateUsername($user);
    updateEmail($user);
    updateShift($user);
    updateState($user);
    updateAdmin($user);
    updateHidden($user);

    if (isset($_POST['resetPW']))
    {
        $user->resetPassword();
    }

    h2("Bearbeite «{$user->name()}»");
    ?>
    <form action="" method="post">
    <input type="submit" name="state" value="<?=$user->isInPause()?"Pause beenden":"In die Pause schicken"?>">
</form>
<form action="" method="post"><p>
    <select name="shift" class="caps">
        <option class="schichtna" selected disabled>Keine Schicht</option>
        <?php
        
        foreach (Shift::getAll() as $shift) {
            $selected = ($user->getCurrentShift() && $user->getCurrentShift()->id() == $shift->id()) ? ' selected' : '';
            print "\t<option class=\"caps, schicht{$shift->id()}\" value=\"{$shift->id()}\"{$selected}>{$shift->label()}\n";
        }
        
        ?>
    </select>
    <input type="submit" value="Speichern"></p>
</form>
<?php
    ?>

    <form action="" method="POST">  
        <input type="text" name="newName" placeholder="Name" value="<?=$user->name()?>">
        <input type="submit" value="Namen ändern">
    </form>

    <form action="" method="POST">
        <input type="text" name="newMail" placeholder="E-Mail" value="<?=$user->email()?>">
        <input type="submit" value="E-Mail ändern">
    </form>
    <form action="" method="post">
        <p>
            <input type="submit" name="resetPW" value="Passwort zurücksetzten (per Mail)">&ensp;
            <input type="submit" name="admin" value="<?=$user->isAdmin()?"De­gra­die­ren":"Befördern"?>">&ensp;
            <input type="submit" name="hidden" value="<?=$user->isHidden()?"Anzeigen":"Verstecken"?>">
        </p>
    </form>
    <form action="" method="post">
        <input type=hidden name="<?=TOKEN?>" value="<?=token()?>">
        <input type="password" name="password" placeholder="Passwort">
        <input type="submit" name="delUser" value="Benutzer löschen">
    </form>

    <?php

    h2("Statuswechsel");

    $linkState = (bool) $_REQUEST['pauseOnly'] + (2 * (bool) $_REQUEST['skipZero']);
    $idLink = '?id=' . $id;
    if ($linkState == 0)
    {
        p ( a($idLink . '&pauseOnly=1', 'Nur Pausen anzeigen')
            . "&emsp;"
            . a($idLink . '&skipZero=1', 'Null-Einträge verstecken'));
    }
    elseif ($linkState == 1)
    {
        p ( a($idLink, 'Anwesend anzeigen')
            . "&emsp;"
            . a($idLink . '&pauseOnly=1'. '&skipZero=1', 'Null-Einträge verstecken'));
    }
    elseif ($linkState == 2)
    {
        p ( a($idLink . '&pauseOnly=1'. '&skipZero=1', 'Nur Pausen anzeigen')
            . "&emsp;"
            . a($idLink, 'Null-Einträge anzeigen'));
    }
    elseif ($linkState == 3)
    {
        p ( a($idLink. '&skipZero=1', 'Anwesend anzeigen')
            . "&emsp;"
            . a($idLink. '&pauseOnly=1', 'Null-Einträge anzeigen'));
    }

    echo "<table><tr><th>Zeit</th><th>Status</th><th>Dauer</th></tr>";

    $lastState = null;

    foreach(UserState::getFromUser($user) as $state)
    {
        $time = $state->time();
        $duration = round(minutesDiff($time, $lastState));

        if ($_REQUEST['skipZero'] && $duration == 0)
        {
            $lastState = $time;
            continue;
        }

        $duration = $state->state()->id() == 2 ? $duration. "′" : round($duration / 6) / 10 . "h";



        if ($_REQUEST['pauseOnly'])
        {
            if ($state->state()->id() == 2)
            {
                echo "<tr><td>", join("</td><td>", [$time, $state->state()->label(), $duration . ""]), "</td></tr>";
            }
        }
        else
        {
            echo "<tr><td>", join("</td><td>", [$time, $state->state()->label(), $duration]), "</td></tr>";
        }
        
        $lastState = $time;
    }

    echo "</table>";

    exit;
}




h2("Benutzer erfassen");

$email = $_POST['email'];
$pw = $_POST['pw'];
$pw2 = $_POST['pw2'];
$name = $_POST['name'];

if (isset($_POST['createUser']) && !notSetOrEmpty($email, $name, $pw, $pw2))
{
    if ($pw == $pw2)
    {
        $res = User::new($email, $name, $pw, isset($_POST['admin']), isset($_POST['hidden']));
        if ($res !== TRUE)
            echo P. $res;
        elseif (User::isValid($email, $pw))
        {
            header('Location: #');
            exit;
        }
    }
    else
        p("Passwörter stimmen nicht überein");
}
elseif (isset($_POST['createUser']))
{
    p("Fülle alle Felder aus");
}

?>

<form action="" method="post">
    <input name="email" type="text" placeholder="E-Mail" style="margin-bottom: 6px">
    <br><input name="name" type="text" placeholder="Name" style="margin-bottom: 6px">
    <br><input name="pw" type="password" placeholder="Passwort" style="margin-bottom: 6px">
    <br><input name="pw2" type="password" placeholder="Passwort wiederholen" style="margin-bottom: 6px">
    <br><input type="checkbox" id="admin" name="admin"><label for="admin">Administrator</label>&emsp;
    <input type="checkbox" id="hidden" name="hidden"><label for="hidden">Versteckt</label>
    <br><input name="createUser" value="Registrieren" type="submit">
</form>
<?php



$tableArray = [
    "Name" => "name",
    "E-Mail" => "email",
    "Schicht Heute" => "shift",
    "Admin" => "admin",
    "Versteckt" => "hidden",
    "" => 'id'
];

$mutateArray = [
    "shift" => function  () use(&$user) {
        return $user->getCurrentShift() ? $user->getCurrentShift()->label() : "-";
    },
    "admin" => function ($x) {
        return $x ? '✓' : '🚫';
        return $x ? '<span class="good">✓</span>' : '<span class="bad">🚫</span>'; },
    "hidden" => function ($x) {
        return $x ? '✓' : '🚫';
        return $x ? '<span class="good">✓</span>' : '<span class="bad">🚫</span>'; },
    "id" => function ($x) { return a ( 'user.php?id='.$x . '&pauseOnly=1', '<strong>✎</strong>' ); }
];

h2("Alle Benutzer");

echo "<p><table>";
echo "<tr><th>", join("</th><th>", array_keys($tableArray)), "</th></tr>";
foreach(User::getAll() as $user)
{
    $filtered = array_filter($user->getValues(true), function ($k) use(&$tableArray){
        return in_array($k, $tableArray);
    }, ARRAY_FILTER_USE_KEY);
    
    echo "<tr><td>", join("</td><td>", array_map(function ($x) use(&$filtered, &$mutateArray) {
        if (array_key_exists($x, $mutateArray))
        {
            return $mutateArray[$x](array_key_exists($x, $filtered) ? $filtered[$x] : $filtered);
        }
        return $filtered[$x];
    }, $tableArray)), "</td></tr>";
}
echo "</table></p>";
