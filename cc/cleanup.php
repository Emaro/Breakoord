<?php

session_start();
define('HOME', '../');
require HOME . 'library.php';

// Redirect guests to the login page
require REDIR_GUESTS;
require REDIR_NADMINS;

if (isset($_POST['cleanup']))
{
    $database->cleanup();
    header("Location: cleanup.php");
}

head();

h1("Datenbank optimieren");

printAdminMenu();

h2("Status");

$size = 0;
$rowCount = 0;

$avrShiftSize = 0;
$avrStateSize = 0;
$shiftCount = UserShift::getOldRecordsCount();
$stateCount = UserState::getOldRecordsCount();

foreach($database->status() as $row)
{
    $size += $row["Data_length"] + $row["Index_length"];
    if ($row['Name'] == 'UserState' || $row['Name'] == 'UserShift')
    {
        $rowCount += $row['Rows'];
    }
    if ($row['Name'] == 'UserState')
    {
        $avrStateSize = $row['Avg_row_length'];
    }
    if ($row['Name'] == 'UserShift')
    {
        $avrShiftSize = $row['Avg_row_length'];
    }
}

p("Datenbankgrösse: " . formatfilesize($size));
p("Anzahl Status und Schichten total: " . $rowCount);

p("Schichten (älter als 1 Jahr): ". $shiftCount. EOL.
  "Anwesenheitsstatus (älter als 1 Jahr): ". $stateCount);


$potential = ($avrStateSize * $stateCount + $avrShiftSize * $shiftCount);
p("Es können mindestens " . formatfilesize($potential) . " eingespart werden.");

h2("Optimieren");

?>

<form action="" method="post">
    <input type="submit" name="cleanup" value="Anwesenheitsstatus und Schichten älter als 1 Jahr löschen!" onclick="return confirm('Alte Datenbankeinträge tatsächlich löschen?')" />
</form>

<p><em>Dieser Vorgang kann <strong>nicht</strong> rückgängig gemacht werden!
<br>Abgesehen von den Auswertungsmöglichkeiten wird die Funktionalität des Pausenmanagers nicht eingeschränkt.
<small>Es kann einen Moment dauern, bis die Datenbankgrösse korrekt angezeigt wird.</small></em>
</p>