-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 17, 2021 at 05:57 PM
-- Server version: 10.6.4-MariaDB-1:10.6.4+maria~hirsute
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `breakoord`
--
CREATE DATABASE IF NOT EXISTS `breakoord` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `breakoord`;

-- --------------------------------------------------------

--
-- Table structure for table `Priority`
--

CREATE TABLE `Priority` (
  `Priority` int(11) NOT NULL,
  `ShiftID` int(11) NOT NULL,
  `StateID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

--
-- Dumping data for table `Priority`
--

INSERT INTO `Priority` (`Priority`, `ShiftID`, `StateID`) VALUES
(3, 1, 1),
(2, 2, 1),
(1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Shift`
--

CREATE TABLE `Shift` (
  `Id` int(11) NOT NULL,
  `Label` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

--
-- Dumping data for table `Shift`
--

INSERT INTO `Shift` (`Id`, `Label`, `Active`) VALUES
(1, 'Früh', 1),
(2, 'Mittel', 1),
(3, 'Spät', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Slots`
--

CREATE TABLE `Slots` (
  `Time` time NOT NULL,
  `Slots` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

-- --------------------------------------------------------

--
-- Table structure for table `State`
--

CREATE TABLE `State` (
  `Id` int(11) NOT NULL,
  `Label` varchar(64) COLLATE utf8mb3_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

--
-- Dumping data for table `State`
--

INSERT INTO `State` (`Id`, `Label`) VALUES
(1, 'Anwesend'),
(2, 'Pause');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `Id` int(11) NOT NULL,
  `Email` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `Name` varchar(255) COLLATE utf8mb3_bin DEFAULT NULL,
  `Hash` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `Admin` int(1) NOT NULL DEFAULT 0,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  `Hidden` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

INSERT INTO `User` (`Id`, `Email`, `Name`, `Hash`, `Admin`, `Active`, `Hidden`) VALUES
(1, 'admin@example.com', 'Admin', '$2y$10$OX1EjPno/CyjO6B58VNe0eT/1bkNOBEvh/19R3f.aaUf0Km5K.oE2', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserShift`
--

CREATE TABLE `UserShift` (
  `Date` date NOT NULL,
  `UserID` int(11) NOT NULL,
  `ShiftID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

-- --------------------------------------------------------

--
-- Table structure for table `UserState`
--

CREATE TABLE `UserState` (
  `Time` datetime NOT NULL,
  `UserID` int(11) NOT NULL,
  `StateID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Priority`
--
ALTER TABLE `Priority`
  ADD PRIMARY KEY (`ShiftID`,`StateID`),
  ADD KEY `FK_PriorityState` (`StateID`);

--
-- Indexes for table `Shift`
--
ALTER TABLE `Shift`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Label` (`Label`);

--
-- Indexes for table `Slots`
--
ALTER TABLE `Slots`
  ADD PRIMARY KEY (`Time`);

--
-- Indexes for table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Label` (`Label`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `UserShift`
--
ALTER TABLE `UserShift`
  ADD PRIMARY KEY (`Date`,`UserID`),
  ADD KEY `FK_ShiftUser` (`UserID`),
  ADD KEY `FK_ShiftShift` (`ShiftID`);

--
-- Indexes for table `UserState`
--
ALTER TABLE `UserState`
  ADD PRIMARY KEY (`Time`,`UserID`),
  ADD KEY `FK_StateUser` (`UserID`),
  ADD KEY `FK_StateState` (`StateID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Shift`
--
ALTER TABLE `Shift`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `State`
--
ALTER TABLE `State`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Priority`
--
ALTER TABLE `Priority`
  ADD CONSTRAINT `FK_PriorityShift` FOREIGN KEY (`ShiftID`) REFERENCES `Shift` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PriorityState` FOREIGN KEY (`StateID`) REFERENCES `State` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `UserShift`
--
ALTER TABLE `UserShift`
  ADD CONSTRAINT `FK_ShiftShift` FOREIGN KEY (`ShiftID`) REFERENCES `Shift` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ShiftUser` FOREIGN KEY (`UserID`) REFERENCES `User` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `UserState`
--
ALTER TABLE `UserState`
  ADD CONSTRAINT `FK_StateState` FOREIGN KEY (`StateID`) REFERENCES `State` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_StateUser` FOREIGN KEY (`UserID`) REFERENCES `User` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
