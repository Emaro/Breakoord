<?php

session_start();
define('HOME', './');
require HOME . 'config.php';
require CONNECTION;
require LIBRARY;


if ($_SESSION[TOKEN] !== $_REQUEST[TOKEN])
{
    header("Location: overview.php");
    exit;
}

// TODO: Change User State

session_destroy();
header('Location: index.php');
exit;
