<?php

function updateUserName(User $user) : bool
{
    $newName = $_POST['newName'];
    
    if (notSetOrEmpty($newName, $user))
        return false;
    
    $user->setName($newName);
    return $user->update();
}

function updateAdmin(User $user) : bool
{
    if (!isset($_POST['admin']))
        return false;
    
    // Do not allow pro-/demotion for the current user
    if (isCurrent($user))
        return false;
    
    $user->setAdmin(!$user->isAdmin());
    $user->update();
    
    if ($user->isAdmin())
        sendPromotionMail($user->email(), currentUser()->name());
    else
        sendDemotionMail($user->email());
    
    return true;
}

function updateHidden(User $user) : bool
{
    if (!isset($_POST['hidden']))
        return false;

    $user->setHidden(!$user->isHidden());
    $user->update();

    return true;
}

function updateEmail(User $user) : bool
{
    $email = $_POST['newMail'];
    
    if (notSetOrEmpty($email, $user))
        return false;
    
    $isCurrent = isCurrent($user);
    
    $user->setEmail($email);
    $success = $user->update();
    
    if ($success && $isCurrent)
        setCurrentEmail($email);
    
    return $success;
}

// Returns the error (or success) message
function updatePassword()
{
    if (empty($_POST['changePassword']))
        return '';
    
    $oldPw = $_POST['oldPw'];
    $newPw = $_POST['newPw'];
    $newPw2 = $_POST['newPw2'];
    
    if (notSetOrEmpty($oldPw, $newPw, $newPw2))
        return "<p>Du musst alle Felder ausfüllen";
    
    if ($newPw !== $newPw2)
        return "<p>Passwörter stimmen nicht überein";
    
    $user = currentUser();
    $ok = $user->setPassword($oldPw, $newPw2);
    
    // TODO: Union all messages at one place
    
    if ($ok !== TRUE)
        return P . $ok;
    
    return $user->update()
        ? "<p>Passwort erfolgreich geändert!"
        : "<p>Es ist ein Fehler aufgetreten";
}

function updateShift(User $user) : bool
{
    $shiftId = $_POST['shift'];
    
    if (notSetOrEmpty($shiftId))
        return false;
    
    if ($shift = Shift::findById($shiftId))
    {
        if ($userShift = UserShift::find(TODAY, $user))
        {
            $userShift->setShift($shift);
            return $userShift->update();
        }
        return UserShift::new(TODAY, $user, $shift);
    }
    return false;
}

function updateState(User $user) : bool
{
    if (empty($_POST['state']))
        return false;
    
    $newStateId = DEFAULT_STATE;
    
    // TODO: Move to User class (getNextState())
    if ($state = $user->getCurrentState())
        $newStateId = ($state->id() == IDLE_STATE) ? PAUSE_STATE : IDLE_STATE;
    
    $numberOfUsersInPause = count(User::inPause());
    
    if ($newStateId == PAUSE_STATE && !$user->isAdmin())
    {
        if ($user->isInPause())
            return false;
        
        $userShift = UserShift::find(TODAY, $user);
        if (is_null($userShift))
            return false;
        
        if (!in_array($userShift->shift()->id(), PARTICIPATING_SHIFTS))
            return false;
        
        if (userSlots() < 1)
            return false;
        
        if ($user->wasInPause())
            return false;
        
        if (PRIORITY_BLOCKS && userSlots() - $numberOfUsersInPause - countCandidates() < 1 && countCandidates() > 0)
            return false;
        
        if ($numberOfUsersInPause >= userSlots())
            return false;
    }
    
    return UserState::new(TIMENOW, $user, State::findById($newStateId));
}