<?php

function countCandidates() : int
{
    $priority = currentUser()->getPriority();
    if ($priority < 0)
        return 0;
    
    $countCandidates = 0;
    
    foreach (User::getAll() as $user)
    {
        if ($user->isHidden())
            continue;
        
        if ($user->wasInPause())
            continue;
        
        if ($user->getPriority() > $priority)
        {
            $countCandidates++;
        }
    }
    
    return $countCandidates;
}

function minutesDiff($then, $now = null)
{
    $now = $now != null ? DateTime::createFromFormat('Y-m-d H:i:s',  $now) : new DateTime();
    $diff = $now->diff(DateTime::createFromFormat('Y-m-d H:i:s',  $then), false); 

    $t = 0;
    $t += $diff->s / 60;
    $t += $diff->i;
    $t += $diff->h * 60;
    $t += $diff->d * 24 * 60;
    return $diff->invert ? $t : -$t;
}

function sameHalfDay($then, $now = null)
{
    $now = $now != null ? DateTime::createFromFormat('Y-m-d H:i:s',  $now) : new DateTime();
    $then = DateTime::createFromFormat('Y-m-d H:i:s',  $then);

    $hourInSeconds = 60 * 60;
    
    $amountOfSecondsIn12Hours = $hourInSeconds * 12;
    
    //  + 2 * 60 * 60 because you need the 2 Hours offset from UTC
    // Update: replaced by getOffset() to address daylight saving time issues
    $tsNow = $now->getTimestamp() + $now->getOffset();
    $tsThen = $then->getTimestamp() + $then->getOffset();
    
    // equals the nth halfday since timestamp == 0
    $hdNow = floor($tsNow / $amountOfSecondsIn12Hours);
    $hdThen = floor($tsThen / $amountOfSecondsIn12Hours);
    
    return $hdNow == $hdThen;
}

function login()
{
    $token = token();
    refreshToken();
    
    // No login requested
    if (empty($_POST['login']))
        return "";

    if ($token != $_POST[TOKEN])
        return "Login fehlgeschlagen.";
    
    $email = strtolower($_POST['email']);
    $password = $_POST['password'];

    if (notSetOrEmpty($email, $password))
        return "Du musst alle Felder ausfüllen.";   

    if (User::isValid($email, $password) !== TRUE)
        return "Passwort vergessen? Wende dich an einen Supervisor.";
    
    // Login successful
    setCurrentEmail($email);
    refreshSite();
}

function pauseAllowed()
{
    $numberOfUsersInPause = count(User::inPause());
    $currentUser = currentUser();
    $currentUserSchicht = UserShift::find(TODAY, $currentUser);
    
    if ($currentUser->isInPause())
    {
        return ['msg' => timerIfInPause()];
    }
    // Wenn keine Schicht hinterlegt ist, Msg & Button verstecken
    elseif (is_null($currentUserSchicht) || !in_array($currentUserSchicht->shift()->id(), PARTICIPATING_SHIFTS))
    {
        return [
            'msg' => "Du hast noch keine Schicht angegeben",
            'dis' => true
        ];
    }
    // Wenn keine Pausenzeit ist, Msg & Button verstecken
    elseif (userSlots() < 1)
    {
        return [
            'msg' => "Es ist keine Pausenzeit",
            'dis' => true
        ];
    }
    // Wenn der User bereits in der Pause war, Msg & Button sperren
    elseif ($currentUser->wasInPause())
    {
        return [
            'msg' => "Du warst bereits in der Pause",
            'dis' => true
        ];
    }
    // Wenn andere User früher in die Pause müssen, Msg & Button sperren
    elseif (userSlots() - $numberOfUsersInPause - countCandidates() < 1 && countCandidates() > 0)
    {
        $countCandidates = countCandidates();
        return [
            'msg' => "Es gibt noch $countCandidates Benutzer, die vor dir in die Pause müssen",
            'dis' => PRIORITY_BLOCKS
        ];
    }
    // Wenn schon zu viele User in der Pause sind, Msg & Button sperren
    elseif ($numberOfUsersInPause >= userSlots())
    {
        return [
            'msg' => "Es sind bereits $numberOfUsersInPause Benutzer in der Pause",
            'dis' => true
        ];
    }
}

function createUserLists()
{
    $can = [];
    $done = [];
    $els = [];

    foreach (User::getAll() as $user)
    {
        if ($user->isInPause() or $user->isHidden())
            continue;
        
        if ($user->wasInPause())
        {
            $done[] = $user;
        }
        elseif ($user->getCurrentShift() == null)
        {
            $els[] = $user;
        }
        else
        {
            $can[] = $user;
        }
    }

    return [
        'can' => $can,
        'done' => $done,
        'else' => $els
    ];
}