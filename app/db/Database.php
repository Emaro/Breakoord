<?php

class Database
{
    private $pdo;

    public function __construct($dbname, $username, $password, $host='localhost')
	{
		$this->pdo = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password);
	}

    // CRUD

    // Create
    public function insert($object)
    {
        $statement = $this->prepare($object::getInsert());
        if ($object->hasValidValues())
        {
            $success = $statement->execute($object->getValues());
            return $success;
        }
        return false;
    }

    // Read
    public function select($object, $query, $args = null)
    {
        $q = $object::getSelect($query);

        $statement = $this->prepare($q);

        $success = $statement->execute($args);

        if ($statement->rowCount() == 0)
            return false;

        while ($record = $statement->fetch())
        {
            $res = $object::create($record, $query);
            yield $res;
        }
    }

    public function status()
    {
        $statement = $this->prepare("SHOW TABLE STATUS");
        $success = $statement->execute();

        while($record = $statement->fetch())
        {
            yield $record;
        }
    }
    
    public function cleanup()
    {
        $statement = $this->prepare("DELETE FROM UserState WHERE Time < DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR)");
        $success = $statement->execute();
        $statement = $this->prepare("DELETE FROM UserShift WHERE Date < DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR)");
        $success = $success && $statement->execute();
        return $success;
    }

    // Update
    public function update($object, $query, $args)
    {
        $statement = $this->prepare($object::getUpdate($query));
        if ($object->hasValidValues())
        {
            return $statement->execute($args);
        }
        return false;
    }

    // Delete
    public function delete($object)
    {
        $statement = $this->prepare($object::getDelete());
        $statement->execute($object->getPrimaryKeys());
    }

    public function truncate($tablename)
    {
        $this->pdo->query("TRUNCATE $tablename");
    }


    // Help functions
    private function prepare ($query)
    {
        return $this->pdo->prepare($query);
    }

}