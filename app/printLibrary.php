<?php

function head($refresh = false)
{
?>
<!DOCTYPE html>


<!-- Meta informations -->

<meta charset="UTF-8">

<?=$refresh?"<meta http-equiv=\"refresh\" content=\"$refresh\">\n":''?>
<title>Pausenmanager</title>

<link rel="icon" type="image/png" sizes="32x32" href="/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/icon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/icon/favicon-16x16.png">

<link rel="stylesheet" href="<?=STYLE?>">


<!-- Site content -->

<?php
}

function printAngemeldetAls($username, $email)
{
    $username = htmlentities($username);
    $email = htmlentities($email);
    p("Angemeldet als: $username ($email)");
}

function printMenu($items, $locals=null)
{
    echo "<nav>" . PHP_EOL;
    
    if ($currentUser = currentUser())
    {
        printAngemeldetAls($currentUser->name(), $currentUser->email());
    }

    p(join("&emsp;", array_filter($items)));
    if ($locals != null)
    {
        p(join("&emsp;", array_filter($locals)));   
    }

    echo "</nav>" . PHP_EOL .PHP_EOL;
}

function p($text, $attr = null)
{
    tag("p", $text, 1, $attr);
}

function h1($title)
{
    tag("h1", $title, 2);
}

function h2($title)
{
    tag("h2", $title, 2);
}

function a($link, $label)
{
    return "<a href=\"$link\">$label</a>";
}

function tag($tag, $content, $brLevel=1, $attr = null)
{
    if ($tag && $content)
        print "<$tag". ($attr ? ' '.$attr: '') .">$content</$tag>" . str_repeat(PHP_EOL, $brLevel);
}

function printMenuCore($local = null)
{
    printMenu([
        a ( HOME . 'overview.php', 'Übersicht' ),
        CCIfAdmin(),
        a ( HOME . 'info.php', 'Info'),
        a ( HOME . 'account.php', 'Account' ),
        CSRFLogout()
    ], $local);
}

function printAdminMenu()
{
    printMenuCore([
        a ( 'analysis.php', 'Auswertung' ),
        a ( 'user.php', 'Benutzer verwalten' ),
        a ( 'slots.php', 'Pausenslots' )
    ]);
}

function printCurrentUserInfos()
{
    print '<p>Dein Status: ' . (currentState() ? (currentState()->label() == 'Pause' ? '' : currentState()->label()) : '-');
    printTimerIfInPause();
    print '</p>';
    p ( 'Deine Schicht heute: ' . (currentShift() ? currentShift()->label() : '-') );
}

function printTimerIfInPause()
{
    print timerIfInPause();
}

function timerIfInPause()
{
    if (currentState() && currentState()->id() == PAUSE_STATE)
    {
        $t = minutesDiff(currentStateTime());

        $rMinutesInPause = (round($t * 5) / 5);
        return '<span id="clock" data-since="'.currentStateTime().'">Du bist seit ' . $rMinutesInPause . ' Minuten in der Pause</span>';
    }
}

function printUserInPauseList()
{
    $nam = array_map('userToShiftNStateTimer', User::inPause());
    sort($nam);
    $lin = join(EOL, $nam);

    echo '  <div id="uList">', PHP_EOL, "    ".P . "Pouse</p>\n    ", $lin ?: '<span class="lspace">-</span>', PHP_EOL, "  </div>", PHP_EOL;
}

function printUserList($list, $title = "")
{
    $nam = array_map('userToShiftNStateTimer', $list);
    sort($nam);

    $title = $title ? P . $title . "</p>" : "";

    $lin = join(EOL. "    ", $nam);

    echo  '  <div>', PHP_EOL, "    ".$title, "\n    ".($lin ?: '<span class="lspace">-</span>'), PHP_EOL, "  </div>", PHP_EOL;
}

function printUserInPauseCounter()
{
    p( 'Anzahl Benutzer in der Pause: <span id="uCount">' . count(User::inPause()) . '</span>/' .userSlots());
}

function userToShiftNStateTimer(User $user)
{
    $today = date('Y-m-d');
    $us = UserShift::find($today, $user);

    $icon = '';

    if ($user->isinPause())
    {
        $t = min(19, round(minutesDiff($user->getCurrentStateTime())));
        $t = intval(floor($t / 5));
        $icon = ' load' . ['99', '66', '33', '00'][$t];
    }
    
    $id = $us ? $us->shift()->id() : 'na';
    $title = $us ? $user->name() . ": " . $us->shift()->label() : 'Keine Schicht';
    $title = htmlentities($title);
    return "<span class='schicht$id$icon lspace' title='$title'>" . $user->name() . "</span>";
}

function pauseButtonLabel()
{
    return currentState() ? (currentState()->id() == IDLE_STATE ? 'Pause' : 'Bin zurück') : 'Pause';
}


function CSRFLogout()
{
    return currentEmail() ? '<a href="'.HOME.'logout.php?token='. token() .'">Abmelden</a>' : null;
}

function CCIfAdmin()
{
    if (currentUser() && currentUser()->isAdmin())
    {
        return '<a href="'.HOME.'cc">Administration</a>';
    }
    return null;
}

function hr()
{
    print "<hr>\n";
}

function printPauseButton($msg, $dis)
{
    ?><form action="" method="post">
  <input type="submit" name="state" value="<?=pauseButtonLabel()?>"<?=currentUser()->isInPause()?'':$dis?>>&emsp;<span id="pause"><?=$msg?></span>
</form>
<?php
}

function printShiftForm()
{
    ?>

<form action="" method="post">
  <p>Deine Schicht: 
    <select name="shift" class="caps">
      <option class="schichtna" selected disabled>Keine Schicht</option>
<?php
        
    $shiftId = currentShift() ? currentShift()->id() : null;
    foreach (Shift::getAll() as $shift)
    {
        $selected = ($shiftId == $shift->id()) ? ' selected' : '';
        print "      "
            . '<option class="caps schicht'
            . $shift->id()
            . '" value="'
            . $shift->id()
            . '"'
            . $selected
            . '>'
            . $shift->label()
            . '</option>'
            . PHP_EOL;
    }
        
?>
    </select>
    <input type="submit" value="Schicht Ändern">    
  </p>
</form>

<?php
}