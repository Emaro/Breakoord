<?php

function userSlots()
{
    return intval(Slot::getCurrentSlot()->slots());
}

function currentState()
{
    return currentUser()->getCurrentState();
}

function currentStateTime()
{
    return currentUser()->getCurrentStateTime();
}

function currentShift()
{
    return currentUser()->getCurrentShift();
}

function currentUserShift()
{
    return UserShift::find(date('Y-m-d'), currentUser());
}

function currentUser()
{
    return User::findByEmail(currentEmail());
}

function isCurrent(User $user) : bool
{
    return isset($user) && $user->email() == currentEmail();
}

function currentEmail()
{
    return $_SESSION[EMAIL] ?: null;
}

function setCurrentEmail($email)
{
    $_SESSION[EMAIL] = $email;
}

function refreshToken($token = TOKEN)
{
    $_SESSION[$token] = bin2hex(random_bytes(TOKEN_LENGTH));
}

function token($token = TOKEN)
{
    return $_SESSION[$token];
}

function notSetOrEmpty(...$args)
{
    foreach ($args as $arg)
    {
        if (empty($arg))
            return true;
    }
    return false;
}

function refreshSite()
{
    header('Location: #');
    exit;
}


function formatfilesize($data) {
    if($data < 1024) {
        return $data . " Bytes";
    }
    else if($data < 1048576) {
        return round(($data / 1024 ), 1) . "&nbsp;KiB";
    }
    else {
        return round(($data / 1048576), 1) . "&nbsp;MiB";
    }
}