<?php

// To show errors and warnings, uncomment these lines.
// To hide errors and warnings, comment these three lines out.

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

// Security headers
header("Content-Security-Policy: default-src 'self'; script-src 'self' 'unsafe-inline';object-src 'none'; style-src 'self' 'unsafe-inline'");
header("X-Frame-Options: SAMEORIGIN");
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Type-Options: nosniff");
header("Referrer-Policy: no-referrer");

define("FROM_MAIL_ADDRESS", 'From: Breakoord <'. SENDER_EMAIL_ADDRESS .'>');

define("PARTICIPATING_SHIFTS", [
    1, 2, 3
]);

define("TOKEN_LENGTH", 32);
define("PHP_DATE_FORMAT", "Y-m-d");
define("REFRESH_INTERVAL", 45); // Not used at the moment

define("TODAY", date('Y-m-d'));
define("TIMENOW", date('Y-m-d H:i:s'));

define("EOL", "<br>" . PHP_EOL);
define("P", "<p>");

define("EMAIL", "user");
define("TOKEN", "token");

$today = date(PHP_DATE_FORMAT);
$days = [null, 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag'];

define("PAUSE_STATE", 2);
define("IDLE_STATE", 1);
define("DEFAULT_STATE", IDLE_STATE);


define("UPDATE_METHODS", HOME . 'app/updateMethods.php');
define("HELP_FUNCTIONS", HOME . 'app/helpFunctions.php');
define("LIBRARY", HOME . "app/printLibrary.php");
define("PRINT_LIB", HOME . "app/printLibrary.php");
define("FUNCTIONS", HOME . "app/funcLibrary.php");
define("CONNECTION", HOME ."app/dbConnection.php");
define("MAILER", HOME . 'app/mailer.php');
define("STYLE", HOME . "css/general.css");

define("REDIR_GUESTS", HOME . 'app/redir/guestGoToLogin.php');
define("REDIR_USERS", HOME . 'app/redir/userGoToGate.php');
define("REDIR_NADMINS", HOME . 'app/redir/nadminGoAway.php');

define("LOGIN", HOME . "newLogin.php");
define("REGISTER", HOME . "newRegister.php");
define("GATE", HOME . "newGate.php");
define("ACC", HOME . "account.php");
define("INFO", HOME . "info.php");