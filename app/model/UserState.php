<?php

class UserState extends Record
{
    private $time;
    private $user;
    private $state;
    protected static $select;
    protected static $insert;
    protected static $update;
    protected static $delete;
    public function __construct($time, $user, $state)
    {
        $this->time = $time;
        $this->user = $user;
        $this->state = $state;
    }

    const ALL = 0;
    const PK = 1;
    const USER = 2;
    const COUNT_OLDER_THAN_A_YEAR = 3;

    public static function init()
    {
        static::$insert = "INSERT INTO UserState (Time, UserId, StateID) VALUES (:time, :user, :state)";

        static::$update = "UPDATE UserState SET StateID = :state WHERE UserID = :user AND Time = :time";

        static::$delete = "DELETE FROM UserState WHERE UserID = :user AND Time = :time";

        $fromAll = function($record) {
            $u = User::findById($record['UserID']);
            $s = State::findById($record['StateID']);

            return new UserState($record['Time'], $u, $s);
        };

        static::$select = [
            self::ALL => [
                S => "SELECT * FROM UserState ORDER BY Time DESC",
                C => $fromAll
            ],
            self::PK => [
                S => "SELECT * FROM UserState WHERE UserID = :user AND Time = :time",
                C => $fromAll
            ],
            self::USER => [
                S => "SELECT * FROM UserState WHERE UserID = :user ORDER BY Time DESC",
                C => $fromAll
            ],
            self::COUNT_OLDER_THAN_A_YEAR => [
                S => "SELECT COUNT(*) FROM UserState WHERE Time < DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR)",
                C => function ($c) { return $c[0]; }
            ]
        ];
    }

    public static function new($time, User $user, State $state)
    {
        return parent::insert(new UserState($time, $user, $state));
    }

    public function getPrimaryKeys()
    {
        return ['time' => $this->time,'user' => $this->user->id()];
    }

    public function hasValidValues()
    {
        if (!isset($this->time, $this->user, $this->state))
        {
            return FALSE;
        }
        
        // TODO: Prüfe, ob User und Status vorhanden sind
        
        return TRUE;
    }

    public function getValues()
    {
        return [
            'time' => $this->time,
            'user' => $this->user->id(),
            'state' => $this->state->id()
        ];
    }

    public static function getAll()
    {
        return self::select('UserState', self::ALL);
    }

    public static function getOldRecordsCount()
    {
        return self::selectFirst('UserState', self::COUNT_OLDER_THAN_A_YEAR, []);
    }

    public static function find($time, $user)
    {
        return self::selectFirst(
            'UserState',
            self::PK,
            ['time' => $time, 'user' => $user->id()]
        );
    }

    public static function getFromUser($user)
    {
        return self::select('UserState', self::USER, [ 'user' => $user->id() ]);
    }

    public function state()
    {
        return $this->state;
    }

    public function user ()
    {
        return $this->user;
    }

    public function time()
    {
        return $this->time;
    }

    public function setState($state)
    {
        $this->state;
    }
}