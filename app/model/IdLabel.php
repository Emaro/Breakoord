<?php

abstract class IdLabel extends Record
{
    protected $id;
    protected $label;

    protected static $insert; // C
    protected static $select; // R
    protected static $update; // U
    protected static $delete; // D

    protected static function getName()
    {
        die("Call this function only with the static keyword");
    }
    
    public function __construct($label, $id = null)
    {
        $this->id = $id;
        $this->label = $label;
    }

    const ALL = 0;
    const BY_ID = 1;
    const BY_LABEL = 2;

    public static function init()
    {
        self::$insert = "INSERT INTO ".static::getName()." (label) VALUES (:label)";

        $fromAll = function($record) {
            $class = static::getName();
            return new $class($record['Label'], $record['Id']);
        };

        self::$select = [

            self::ALL => [
                S => "SELECT * FROM ".static::getName(),
                C => $fromAll
            ],

            self::BY_ID => [
                S => "SELECT * FROM ".static::getName()." WHERE id = :id",
                C => $fromAll
            ],

            self::BY_LABEL => [
                S => "SELECT * FROM ".static::getName()." WHERE label = :label",
                C => $fromAll
            ]
        ];
    }

    public static function insert($label)
    {
        $class = static::getName();
        parent::insert(new $class($label));
    }

    public function hasValidValues()
    {
        // TODO: Validate
        return isset($this->label);
    }

    public function getValues()
    {
        return [
            'id' => $this->id,
            'label' => $this->label
        ];
    }

    public static function getAll()
    {
        return self::select(static::getName(), self::ALL);
    }

    public static function findById($id): State
    {
        return self::selectFirst(static::getName(), self::BY_ID, ['id' => $id]);
    }

    public function id()
    {
        return $this->id;
    }

    public function label()
    {
        return $this->label;
    }
}
