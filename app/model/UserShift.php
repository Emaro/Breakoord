<?php

class UserShift extends Record
{
    private $date;
    private $user;
    private $shift;
    protected static $select;
    protected static $update;
    protected static $insert;
    protected static $delete;
    public function __construct($date, $user, $shift)
    {
        $this->date = $date;
        $this->user = $user;
        $this->shift = $shift;
    }

    const ALL = 0;
    const PK = 1;
    const COUNT_OLDER_THAN_A_YEAR = 2;

    public static function init()
    {
        static::$insert = "INSERT INTO UserShift (Date, UserId, ShiftID) VALUES (:date, :user, :shift)";

        static::$update = "UPDATE UserShift SET ShiftID = :shift WHERE UserID = :user AND Date = :date";

        static::$delete = "DELETE FROM UserShift WHERE UserID = :user AND Date = :date";
        
        $fromAll = function($record) {
            $u = User::findById($record['UserID']);
            $s = Shift::findById($record['ShiftID']);
            return new UserShift($record['Date'], $u, $s);
        };

        static::$select = [
            self::ALL => [
                S => "SELECT * FROM UserShift",
                C => $fromAll
            ],
            self::PK => [
                S => "SELECT * FROM UserShift WHERE UserID = :user AND Date = :date",
                C => $fromAll
            ],
            self::COUNT_OLDER_THAN_A_YEAR => [
                S => "SELECT COUNT(*) FROM UserShift WHERE Date < DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR)",
                C => function ($c) { return $c[0]; }
            ]
        ];
    }

    public static function new($date, $user, $shift)
    {
        return parent::insert(new UserShift($date, $user, $shift));
    }

    public function getPrimaryKeys()
    {
        return ['date' => $this->date,'user' => $this->user->getPrimaryKeys()['id']];
    }

    public function hasValidValues()
    {
        if (!isset($this->date, $this->user, $this->shift))
        {
            return FALSE;
        }
        
        // TODO: Prüfe, ob User und Schicht vorhanden sind
        
        return TRUE;
    }

    public function getValues()
    {
        return array('date' => $this->date, 'user' => $this->user->id(), 'shift' => $this->shift->id());
    }

    public static function getAll()
    {
        return self::select('UserShift', self::ALL, []);
    }

    public static function getOldRecordsCount()
    {
        return self::selectFirst('UserShift', self::COUNT_OLDER_THAN_A_YEAR, []);
    }

    public static function find($date, User $user)
    {
        return self::selectFirst('UserShift', self::PK, ['date' => $date, 'user' => $user->id()]);
    }

    // Getter

    public function shift() : Shift
    {
        return $this->shift;
    }

    public function user ()
    {
        return $this->user;
    }

    public function date()
    {
        return $this->date;
    }

    public function setShift($shift)
    {
        $this->shift = $shift;
    }
}