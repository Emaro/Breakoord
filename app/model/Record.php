<?php

define("S", "SELECT");
define("C", "CREATE");

abstract class Record
{
    private static $database;

    protected static $insert; // C
    protected static $select; // R
    protected static $update; // U
    protected static $delete; // D

    protected abstract function getValues();

    public static function setDatabase(Database $database)
    {
        self::$database = $database;
    }

    public static function getDatabase() : Database
    {
        return self::$database;
    }

    public static function getInsert()
    {
        return static::$insert;
    }

    public static function getSelect($query)
    {
        if (array_key_exists($query, static::$select))
            return static::$select[$query][S];
        return null;
    }

    public static function getDelete()
    {
        return static::$delete;
    }

    public static function getUpdate($query)
    {
        return static::$update;
    }

    public static function create($record, $query)
    {
        $res = static::$select[$query][C]($record);
        return $res;
    }

    protected static function insert(Record $record)
    {
        return self::getDatabase()->insert($record);
    }

    protected static function select($object, $query, $args = [])
    {
        $res = self::getDatabase()->select($object, $query, $args);
        return $res;
    }

    protected static function selectFirst($object, $query, $args = [])
    {
        $res = self::select($object, $query, $args);
        return $res->current();
    }
    
    protected static function deleteFromDB($object)
    {        
        self::getDatabase()->delete($object);
    }

    public function update()
    {
        return self::getDatabase()->update($this, null, $this->getValues(true));
    }

    public function delete()
    {
        if (static::$delete)
        {
            self::deleteFromDB($this);
        }
    }
}