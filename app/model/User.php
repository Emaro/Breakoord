<?php

class User extends Record
{
    private $id;
    private $email;
    private $name;
    private $hash;
    private $admin;
    private $active;
    private $hidden;

    protected static $insert; // C
    protected static $select; // R
    protected static $update; // U
    protected static $delete; // D

    public function __construct($id, $email, $name, $hash, $admin, $hidden = 0)
    {
        $this->id = $id;
        $this->email = strtolower($email);
        $this->name = $name;
        $this->hash = $hash;
        $this->admin = $admin;
        $this->hidden = $hidden;
    }


    const ALL = 0;
    const BY_EMAIL = 1;
    const BY_ID = 11;
    const SHIFTS_BY_EMAIL = 2;
    const STATE_BY_EMAIL = 3;
    const STATE_TIME = 4;
    const BY_STATE = 12;
    const VISIBLE_ONLY = 90;
    const HIDDEN_ONLY = 91;

    public static function init()
    {
        static::$insert = "INSERT INTO User (email, name, hash, admin, hidden) VALUES (:email, :name, :hash, :admin, :hidden)";

        static::$update = "UPDATE User SET email = :email,
            name = :name,
            hash = :hash,
            admin = :admin,
            hidden = :hidden WHERE id = :id";

        //static::$delete = "DELETE FROM User WHERE id = :id";
        static::$delete = "UPDATE User SET Active = 0 WHERE id = :id";
        
        $fromAll = function($record) {
            return new User($record['Id'], $record['Email'], $record['Name'], $record['Hash'], $record['Admin'], $record['Hidden']);
        };
        $shift = function($record) {
            return new Shift($record['Label'], $record['Id']);
        };
        $state = function($record)
        {
            return new State($record['Label'], $record['Id']);
        };
        $timeFunc = function($record) { 
            return $record['Time']; };

        static::$select = [
            
            self::ALL => [
                S => "SELECT * FROM User WHERE Active = 1",
                C => $fromAll
            ],

            self::BY_EMAIL => [
                S => "SELECT * FROM User WHERE email = :email AND Active = 1",
                C => $fromAll
            ],

            self::BY_ID => [
                S => "SELECT * FROM User WHERE id = :id AND Active = 1",
                C => $fromAll
            ],

            self::BY_STATE => [
                S => "SELECT * FROM State JOIN UserState ON State.Id = UserState.StateId JOIN User ON User.Id = UserState.UserId WHERE State.Id = :state AND Active = 1 ORDER BY UserState.Time DESC",
                C => $fromAll
            ],
            
            self::SHIFTS_BY_EMAIL => [
                S => "SELECT * FROM Shift JOIN UserShift ON Shift.Id = UserShift.ShiftId JOIN User ON User.Id = UserShift.UserId WHERE User.Email = :email AND Active = 1 ORDER BY UserShift.Date DESC",
                C => $shift
            ],

            self::STATE_BY_EMAIL => [
                S => "SELECT State.Id, State.Label FROM State JOIN UserState ON State.Id = UserState.StateId JOIN User ON User.Id = UserState.UserId WHERE User.Email = :email AND Active = 1 ORDER BY UserState.Time DESC",
                C => $state
            ],

            self::STATE_TIME => [
                S => "SELECT UserState.Time FROM UserState JOIN User ON User.Id = UserState.UserId WHERE User.Email = :email AND Active = 1 ORDER BY UserState.Time DESC",
                C => $timeFunc
            ],

            self::VISIBLE_ONLY => [
                S => "SELECT * FROM User WHERE Active = 1 AND Hidden = 0",
                C => $fromAll
            ],

            self::HIDDEN_ONLY => [
                S => "SELECT * FROM User WHERE Active = 1 AND Hidden = 1",
                C => $fromAll
            ],
        ];
    }

    public static function new(string $email, string $name, string $pw, $admin = 0, $hidden = 0)
    {
        $email = strtolower($email);
        
        $user = self::findByEmail($email);
        if ($user != NULL)
        {
            return "Diese E-Mail-Adresse wird bereits verwendet";
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('/@bit.admin.ch/', $email))
            return "Es muss eine gültige BIT-Adresse verwendet werden (x.y@bit.admin.ch).";

        if (!self::matchesPWRule($pw))
            return "Das neue Passwort muss mindestens ".PW_MIN_LENGTH." Zeichen lang sein und drei der vier Kategorien Kleinbuchstaben, Grossbuchstaben, Zahlen und Sonderzeichen enthalten.";
        
        $hash = password_hash($pw, PASSWORD_DEFAULT);

        $user = new User(null, strtolower($email), $name, $hash, $admin, $hidden);
        return parent::insert($user);
    }
    
    public function hasValidValues()
    {
        // TODO: Validate
        return isset($this->email, $this->name, $this->hash, $this->admin, $this->hidden);
    }
    
    public function getValues($withId = false)
    {
        $arr = [
            'email' => $this->email,
            'name' => $this->name,
            'hash' => $this->hash,
            'admin' => $this->admin,
            'hidden' => $this->hidden
        ];
        if ($withId)
        {
            $arr['id'] = $this->id;
        }

        return $arr;
    }


    public static function isValid($email, $pw)
    {
        $user = self::selectFirst('User', self::BY_EMAIL, ['email' => $email]);

        if (!$user)
            return false;
        
        return $user->isValidPassword($pw);
    }

    public function isValidPassword($pw)
    {
        if (!$this->hash)
            return false;
        
        if (!$pw)
            return false;
        
        return password_verify($pw, $this->hash);
    }

    public static function getAll()
    {
        return self::select('User', self::ALL, []);
    }

    public static function findByEmail($email)
    {
        return self::selectFirst('User', self::BY_EMAIL, ['email' => $email]);
    }

    public static function findById($id)
    {
        return self::selectFirst('User', self::BY_ID, ['id' => $id]);
    }

    public function getPrimaryKeys()
    {
        return ['id' => $this->id];
    }

    public function setEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match('/@bit.admin.ch/', $email))
            return "Es muss eine gültige BIT-Adresse verwendet werden (x.y@bit.admin.ch).";
        
        $this->email = strtolower($email);
        return true;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPassword($oldPw, $pw)
    {
        if (!self::matchesPWRule($pw))
        {
            $minLength = PW_MIN_LENGTH;
            return "Das neue Passwort muss mindestens $minLength Zeichen lang sein und drei der vier Kategorien Kleinbuchstaben, Grossbuchstaben, Zahlen und Sonderzeichen enthalten.";
        }
            

        if ($this->isValidPassword($oldPw))
        {
            $this->hash = password_hash($pw, PASSWORD_DEFAULT);
            return true;
        }
        return "Das alte Passwort ist nicht korrekt.";
    }

    public function resetPassword()
    {
        $pw = base_convert(bin2hex(random_bytes(12)), 16, 36);
        $this->hash = password_hash($pw, PASSWORD_DEFAULT);
        $this->update();

        $subject = "Passwort Reset";
        $text = "Dein Passwort wurde von einem Administrator zurückgesetzt. Bitte melde dich möglichst schnell an und ändere dein Passwort. Diese E-Mail wurde unverschlüsselt verschickt, was die Sicherheit deines Kontos beeinträchtigt.\n\nTemporäres Passwort: " . $pw;
        
        mail($this->email(), $subject, $text, FROM_MAIL_ADDRESS);
    }

    public function getShifts()
    {
        //return null;
        return parent::select('User', self::SHIFTS_BY_EMAIL, ['email' => $this->email]);
    }

    public function getCurrentShift()
    {
        $userShift = UserShift::find(date('Y-m-d'), $this);
        return $userShift ? $userShift->shift() : null;
        return parent::selectFirst('User', self::SHIFTS_BY_EMAIL, ['email' => $this->email]);
    }

    public function getCurrentState() : State
    {
        return parent::selectFirst('User', self::STATE_BY_EMAIL, ['email' => $this->email()]) ?? State::findById(IDLE_STATE);
    }

    public function getCurrentStateTime()
    {
        $c =  parent::selectFirst('User', self::STATE_TIME, ['email' => $this->email()]);
        return $c;
    }

    public function getPriority()
    {
        $uState = $this->getCurrentState();
        $uShift = $this->getCurrentShift();
        if (isset($uState, $uShift))
        {   
            if ($p = Priority::find($uShift, $uState) and $p != null)
            {
                return $p->priority();
            }
        }
        return -1;
    }

    public function isInPause()
    {
        return $this->getCurrentState() ? $this->getCurrentState()->id() == PAUSE_STATE : false;
    }

    public function wasInPause()
    {
        $lastState = null;

        foreach(UserState::getFromUser($this) as $state)
        {
            $time = $state->time();
            
            if ($state->state()->id() != PAUSE_STATE)
            {
                $lastState = $time;
                continue;
            }

            if (!sameHalfDay($time))
            {
                $lastState = $time;
                continue;
            }
            
            if (minutesDiff($time) <= 0)
            {
                $lastState = $time;
                continue;
            }
            
            $duration = round(minutesDiff($time, $lastState));
            
            if ($duration >= 5)
            {
                return true;
            }
            
            $lastState = $time;
        }
        return false;
    }

    public function email()
    {
        return $this->email;
    }

    public function name()
    {
        return $this->name;
    }

    public function id()
    {
        return $this->id;
    }

    public static function inPause()
    {
        $users = [];
        foreach (self::getAll() as $user)
        {
            if ($user->getCurrentState() ? $user->getCurrentState()->id() == PAUSE_STATE : false)
            {
                $users[] = $user;
            }
        }
        return $users;
    }

    public function isAdmin()
    {
        return (bool) $this->admin;
    }

    public function setAdmin($bool)
    {
        $this->admin = $bool ? 1 : 0;
    }

    public function isHidden()
    {
        return (bool) $this->hidden;
    }

    public function setHidden($bool)
    {
        $this->hidden = $bool ? 1 : 0;
    }

    public static function matchesPWRule($pw)
    {
        if (strlen($pw) < PW_MIN_LENGTH)
            return false;
        
        $sum = preg_match('/[a-z]/', $pw)
            + preg_match('/[A-Z]/', $pw)
            + preg_match('/[0-9]/', $pw)
            + preg_match('/[\W_]/', $pw);

        return $sum >= 3;
    }

}