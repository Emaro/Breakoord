<?php

require_once HOME . "app/model/IdLabel.php";

class State extends IdLabel
{
    protected static function getName()
    {
        return "State";
    }
}