<?php

class Shift extends Record
{
    private $id;
    private $label;

    protected static $select;
    protected static $insert;
    protected static $delete;
    
    public function __construct($label, $id = null)
    {
        $this->id = $id;
        $this->label = $label;
    }

    const ALL = 0;
    const BY_ID = 1;
    const BY_LABEL = 2;

    public static function init()
    {
        self::$insert = "INSERT INTO Shift (Label) VALUES (:label)";

        self::$delete = "UPDATE Shift SET Active = 0 WHERE Id = :id";

        $fromAll = function($record) {
            return new Shift($record['Label'], $record['Id']);
        };

        self::$select = [

            self::ALL => [
                S => "SELECT * FROM Shift",
                C => $fromAll
            ],

            self::BY_ID => [
                S => "SELECT * FROM Shift WHERE Id = :id",
                C => $fromAll
            ],

            self::BY_LABEL => [
                S => "SELECT * FROM Shift WHERE Label = :label",
                C => $fromAll
            ]
        ];
    }

    public static function insert($label)
    {
        return parent::insert(new Shift($label));
    }

    public function hasValidValues()
    {
        // TODO: Validate
        return isset($this->label);
    }

    public function getPrimaryKeys()
    {
        return ['id' => $this->id];
    }

    public static function findById($id) : Shift
    {
        return self::selectFirst('Shift', self::BY_ID, ['id' => $id]);
    }

    public function getValues()
    {
        return [
            'label' => $this->label
        ];
    }

    public function id()
    {
        return $this->id;
    }

    public function label()
    {
        return $this->label;
    }

    public static function getAll()
    {
        return self::select('Shift', self::ALL);
    }
}