<?php

class Priority extends Record
{
    private $priority;
    private $shift;
    private $state;

    protected static $select;
    protected static $insert;
    protected static $update;
    protected static $delete;

    public function __construct($priority, $shift, $state)
    {
        $this->priority = $priority;
        $this->shift = $shift;
        $this->state = $state;
    }

    const ALL = 0;
    const PK = 1;

    public static function init()
    {
        static::$insert = "INSERT INTO Priority (Priority, ShiftId, StateID) VALUES (:priority, :shift, :state)";

        static::$update = "UPDATE Priority SET Priority = :priority WHERE ShiftID = :shift AND StateID = :state";

        static::$delete = "DELETE FROM Priority WHERE ShiftID = :shift AND StateID = :state";

        $fromAll = function($record) {
            $shift = Shift::findById($record['ShiftID']);
            $state = State::findById($record['StateID']);

            return new Priority($record['Priority'], $shift, $state);
        };

        static::$select = [
            self::ALL => [
                S => "SELECT * FROM Priority",
                C => $fromAll
            ],
            self::PK => [
                S => "SELECT * FROM Priority WHERE ShiftID = :shift AND StateID = :state",
                C => $fromAll
            ]
        ];
    }

    public static function new($priority, $shift, $state)
    {
        return parent::insert(new Priority($priority, $shift, $state));
    }

    public function getPrimaryKeys()
    {
        return ['shift' => $this->shift->id(),'state' => $this->state->id()];
    }

    public function hasValidValues()
    {
        if (!isset($this->priority, $this->shift, $this->state))
        {
            return FALSE;
        }
        
        // TODO: Prüfe, ob Shift und State vorhanden sind
        
        return TRUE;
    }

    public function getValues()
    {
        return [
            'priority' => $this->priority,
            'shift' => $this->shift->id(),
            'state' => $this->state->id()
        ];
    }

    public static function getAll()
    {
        return self::select('Priority', self::ALL);
    }

    public static function find($shift, $state)
    {
        return self::selectFirst(
            'Priority',
            self::PK,
            ['shift' => $shift->id(), 'state' => $state->id()]
        );
    }

    public function state()
    {
        return $this->state;
    }

    public function shift ()
    {
        return $this->shift;
    }

    public function priority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
}