<?php

class Slot extends Record
{
    private $time;
    private $slots;

    protected static $insert; // C
    protected static $select; // R
    protected static $update; // U
    protected static $delete; // D

    public function __construct($time, $slots)
    {
        $this->time = $time;
        $this->slots = $slots;
    }

    const ALL = 0;
    const PK = 1;
    const CURRENT = 2;

    public static function init()
    {
        static::$insert = "INSERT INTO Slots (Time, Slots) VALUES (:time, :slots)";

        static::$update = "UPDATE Slots SET Time = :time, Slots = :slots WHERE Time = :time";

        static::$delete = "DELETE FROM Slots WHERE Time = :time";

        $fromAll = function($record) {
            return new Slot($record['Time'], $record['Slots']);
        };

        static::$select = [
            self::ALL => [
                S => "SELECT * FROM Slots ORDER BY Time ASC",
                C => $fromAll
            ],
            self::PK => [
                S => "SELECT * FROM Slots WHERE Time = :time ORDER BY Time ASC",
                C => $fromAll
            ],
            self::CURRENT => [
                S => "SELECT * FROM Slots WHERE Time <= CURRENT_TIME() ORDER BY Time DESC LIMIT 1",
                C => $fromAll
            ]
        ];
    }

    public static function new($time, $slots)
    {
        return parent::insert(new Slot($time, $slots));
    }

    public function getPrimaryKeys()
    {
        return ['time' => $this->time()];
    }

    public function hasValidValues()
    {
        if (!isset($this->time, $this->slots))
        {
            return FALSE;
        }
        
        return TRUE;
    }

    public function getValues()
    {
        return [
            'time' => $this->time(),
            'slots' => $this->slots()
        ];
    }

    public static function getAll()
    {
        return self::select('Slot', self::ALL);
    }

    public static function find($time)
    {
        return self::selectFirst(
            'Slot',
            self::PK,
            ['time' => $time]
        );
    }

    public static function getCurrentSlot()
    {
        return self::selectFirst('Slot', self::CURRENT) ?: new Slot('00:00:00', 0);
    }

    public function time()
    {
        return $this->time;
    }

    public function slots()
    {
        return $this->slots;
    }
}