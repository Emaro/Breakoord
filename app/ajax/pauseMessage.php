<?php

session_start();

define('HOME', '../');

require HOME . 'config.php';
require CONNECTION;
require LIBRARY;

$email = $_SESSION['user'];
if (!isset($email))
{
    header("Location: newLogin.php");
    exit;
}

$currentUser = User::findByEmail($email);

if ($currentUser->statusId() != 3)
{
    print '<p id="clock" data-since="">Du bist gerade nicht in der Pause';
}
else
{
    print '<p id="clock" data-since="{$currentUser->lastUpdate()}">Du bist seit ' . (round($currentUser->getPauseTime() * 5) / 5) . ' Minuten in der Pause';
}