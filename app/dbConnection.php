<?php

require_once HOME . 'app/db/Database.php';

$database = new Database(DB_NAME, DB_USER, DB_PASS);
// Example if the database is not located on localhost:
// $database = new Database(DB_NAME, DB_USER, DB_PASS, 'www.location-of.my.database.example.com/pausenmanager_db');
// Be aware, that still a MySQL database is expected on the provided endpoint.

$classes = [
    'Shift',
    'State',
    'User',
    'UserShift',
    'UserState',
    'Priority',
    'Slot'
];

require_once HOME . 'app/model/Record.php';

foreach ($classes as $c)
{
    require_once HOME . 'app/model/' . $c . '.php';
}

Record::setDatabase($database);

foreach ($classes as $c)
{
    $c::init();
}