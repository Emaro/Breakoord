<?php

function sendPromotionMail($email, $name)
{
    mail (
        $email,
        'Beförderung',
        "Gratulation\n\nDu wurdest von $name zum Administrator ernannt. Die Änderung ist per sofort wirksam.",
        FROM_MAIL_ADDRESS
    );
}

function sendDemotionMail($email)
{
    mail (
        $email,
        'Administrator-Rechte enzogen',
        'Dir wurden alle Admin-Rechte entzogen. Du kannst nur noch als gewöhnlicher Benutzer agieren.',
        FROM_MAIL_ADDRESS
    );
}