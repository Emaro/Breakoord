<?php

if (!isset($targetLocation))
{
    die('ERROR: Target location is not set');
}
elseif (empty($targetLocation))
{
    die('ERROR: Target location is empty');
}
else
{
    header('Location: ' . $home . $targetLocation);
    unset($targetLocation);
}