<?php

$user = $_SESSION[EMAIL];

if (!currentUser()->isAdmin())
{
    $targetLocation = 'index.php';
    if (HOME)
        require HOME . 'app/redir/redirectToTargetLocation.php';
    else
        require $home . 'app/redir/redirectToTargetLocation.php';
}